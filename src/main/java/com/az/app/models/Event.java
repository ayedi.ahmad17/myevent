package com.az.app.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="evenement")
public class Event implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id  @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idEvent;
	private int nbparticipant;

	
	private String sport;
	
	private double prix;
	 
	private String adresse;
	
	private int nbplace;
	
	private String heure_debut;

	private String niveau;
	@JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate date;

	private double duree;

	@OneToOne
	@JoinColumn(name = "id_createur", referencedColumnName = "user_id" )
	private User createur;

	@ManyToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinTable(name = "User_event",
	joinColumns = {
			@JoinColumn(name = "event_id",referencedColumnName = "idEvent",nullable = false, updatable = false)},
	inverseJoinColumns = {
			@JoinColumn(name = "user_id",referencedColumnName = "user_id",nullable = false, updatable = false)})

	private Set<User> participants = new HashSet<User>();
	
	public Event() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Event(long idEvent, int nbparticipant, String sport, double prix, String adresse, int nbplace,
			String heure_debut, String niveau, LocalDate date, double duree, User createur) {
		super();
		this.idEvent = idEvent;
		this.nbparticipant = nbparticipant;
		this.sport = sport;
		this.prix = prix;
		this.adresse = adresse;
		this.nbplace = nbplace;
		this.heure_debut = heure_debut;
		this.niveau = niveau;
		this.date = date;
		this.duree = duree;
		this.createur = createur;
	}
	public int getNbparticipant() {
		return nbparticipant;
	}

	public void setNbparticipant(int nbparticipant) {
		this.nbparticipant = nbparticipant;
	}

	public String getAdresse() {
		return adresse;
	}
	public String getNiveau() {
		return niveau;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}

	public long getIdEvent() {
		return idEvent;
	}
	public void setIdEvent(long idEvent) {
		this.idEvent = idEvent;
	}
	public String getSport() {
		return sport;
	}
	public void setSport(String sport) {
		this.sport = sport;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getNbplace() {
		return nbplace;
	}
	public void setNbplace(int nbplace) {
		this.nbplace = nbplace;
	}
	public String getHeure_debut() {
		return heure_debut;
	}
	public void setHeure_debut(String heure_debut) {
		this.heure_debut = heure_debut;
	}
	public double getDuree() {
		return duree;
	}
	public void setDuree(double duree) {
		this.duree = duree;
	}
	public User getCreateur() {
		return createur;
	}
	public void setCreateur(User createur) {
		this.createur = createur;
	}
	public Set<User> getParticipants() {
		return participants;
	}
	public void setParticipants(Set<User> participants) {
		this.participants = participants;
	}
	@Override
	public String toString() {
		return "Event [idEvent=" + idEvent + ", sport=" + sport + ", prix=" + prix + ", nbplace=" + nbplace
				+ ", niveau=" + niveau + ", date=" + date + ", heure_debut=" + heure_debut + ", duree=" + duree
				+ ", createur=" + createur + ", participants=" + participants + "]";
	}







}
