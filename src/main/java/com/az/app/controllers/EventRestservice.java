package com.az.app.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.az.app.models.Event;
import com.az.app.models.User;
import com.az.app.payload.request.EventRequest;
import com.az.app.payload.response.MessageResponse;
import com.az.app.repository.EventRepository;
import com.az.app.repository.UserRepository;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/api/auth/event")
public class EventRestservice {

	@Autowired
	private EventRepository eventrep;
	@Autowired
	private UserRepository userrepo;

	@GetMapping("/allevents")
	public List<Event> getEvent(){
		return eventrep.findAll();
	}
	@GetMapping("/events/{id}")
	public Event getEvent(@PathVariable Long id){
		Optional <Event> event = eventrep.findById(id);
		return event.get();
	}
	@PutMapping("/events/ajout/{id}/{idu}")
	public Event AjoutUserToEvent(@PathVariable Long id,@PathVariable Long idu,@RequestBody Event e){
		User u1=userrepo.findById(idu).orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: "));
		if(e.getCreateur().getUser_id()==u1.getUser_id()) 
		{
			throw new ResourceNotFoundException("le createur est deja un participant ");
		}
		else if(e.getNbparticipant()>=e.getNbplace())
		{
			throw new ResourceNotFoundException("nombre max des participants ");
		}
		else if( e.getParticipants().contains(u1))
		{
			throw new ResourceNotFoundException("participant inscrit ");
		}
		else 
		{e.getParticipants().add(u1);
		e.setNbparticipant(e.getNbparticipant()+ 1) ;}

		return	eventrep.save(e);}


	@PostMapping("/events")
	public ResponseEntity<?> registerUser(@Valid @RequestBody EventRequest EventRequest) {
		// Create new event's account
		Event event = new Event(
				EventRequest.getIdEvent(),
				1,
				EventRequest.getSport(),
				EventRequest.getPrix(),
				EventRequest.getAdresse(),
				EventRequest.getNbplace(),
				EventRequest.getHeure_debut(),
				EventRequest.getNiveau(),
				EventRequest.getDate(),
				EventRequest.getDuree(),
				userrepo.findById((long) EventRequest.getId_createur()).orElseThrow(null)
				);
		eventrep.save(event);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@DeleteMapping("/events/{id}")
	public boolean deleteEvent(@PathVariable Long id) {
		eventrep.deleteById(id);

		return true;
	}
	@PutMapping("/events/remove/{id}/{idu}")
	public Event deleteuserfromevent(@PathVariable Long id,@PathVariable Long idu,@RequestBody Event e) {
		User u1=userrepo.findById(idu).orElseThrow(() -> new ResourceNotFoundException("event not found for this id :: "));
		if(e.getCreateur().getUser_id()==u1.getUser_id()) 
		{
			throw new ResourceNotFoundException("le createur NE PEUT PAS se désinscrire ");
		}
		e.setNbparticipant(e.getNbparticipant()-1) ;
		e.getParticipants().remove(u1);

		return	eventrep.save(e);
	}

	@PutMapping("/events/{id}")
	public Event updateEvent(@PathVariable Long id,@RequestBody Event e)throws ResourceNotFoundException {{

		Event e1 = eventrep.findById(id).orElseThrow(() -> new ResourceNotFoundException("Event not found for this id :: " + id));
		e1.setSport(e.getSport());
		e1.setPrix(e.getPrix());
		e1.setNbplace(e.getNbplace());
		e1.setHeure_debut(e.getHeure_debut());
		e1.setDuree(e.getDuree());
		e1.setParticipants(e.getParticipants());

		return eventrep.save(e1);
	}}
	@GetMapping("/ec/{id}")
	public List<Event> getEventByuserc(@PathVariable Long id){
		User u1=userrepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: "));
		List<Event> events= new ArrayList<Event>();

		eventrep.findAll().forEach(c->{if (c.getCreateur().equals(u1))

			events.add(c);
		} );
		return events ;
	}
	@GetMapping("/ep/{id}")
	public List<Event> getEventByuserp(@PathVariable Long id){
		User u1=userrepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: "));
		List<Event> events= new ArrayList<Event>();

		eventrep.findAll().forEach(c->{if (c.getParticipants().contains(u1))

			events.add(c);
		} );
		return events ;
	}
	

}

