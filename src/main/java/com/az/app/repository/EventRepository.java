package com.az.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.az.app.models.Event;
@Repository
public interface EventRepository extends JpaRepository <Event,Long>{

}
