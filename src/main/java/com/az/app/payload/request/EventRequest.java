package com.az.app.payload.request;

import java.time.LocalDate;

public class EventRequest {
   
private long idEvent;
private String sport;
private double prix;
private int nbplace;
private String niveau;
private String adresse;
private LocalDate date;
private String heure_debut;
private double id_createur;
private int nbparticipant;



public int getNbparticipant() {
	return nbparticipant;
}
public void setNbparticipant(int nbparticipant) {
	this.nbparticipant = nbparticipant;
}
public String getAdresse() {
	return adresse;
}
public void setAdresse(String adresse) {
	this.adresse = adresse;
}
public double getId_createur() {
	return id_createur;
}
public void setId_createur(double id_createur) {
	this.id_createur = id_createur;
}
public String getHeure_debut() {
	return heure_debut;
}
public double getDuree() {
	return duree;
}
public void setHeure_debut(String heure_debut) {
	this.heure_debut = heure_debut;
}
public void setDuree(double duree) {
	this.duree = duree;
}
private double duree;
public long getIdEvent() {
	return idEvent;
}
public String getSport() {
	return sport;
}
public double getPrix() {
	return prix;
}
public int getNbplace() {
	return nbplace;
}
public String getNiveau() {
	return niveau;
}
public LocalDate getDate() {
	return date;
}
public void setIdEvent(long idEvent) {
	this.idEvent = idEvent;
}
public void setSport(String sport) {
	this.sport = sport;
}
public void setPrix(double prix) {
	this.prix = prix;
}
public void setNbplace(int nbplace) {
	this.nbplace = nbplace;
}
public void setNiveau(String niveau) {
	this.niveau = niveau;
}
public void setDate(LocalDate date) {
	this.date = date;
}
}